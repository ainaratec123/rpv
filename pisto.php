<!DOCTYPE html>
<html lang="en"><!-- Basic -->
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">   
   
    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
 
     <!-- Site Metas -->
    <title>Live Dinner Restaurant - Responsive HTML5 Template</title>  
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Site Icons -->
    <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
    <link rel="apple-touch-icon" href="images/apple-touch-icon.png">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">    
	<!-- Site CSS -->
    <link rel="stylesheet" href="css/style.css">    
    <!-- Responsive CSS -->
    <link rel="stylesheet" href="css/responsive.css">
    <!-- Custom CSS -->
    <link rel="stylesheet" href="css/custom.css">

    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->


	<style type="text/css">
		.estrellas {text-shadow: 0 0 1px #F48F0A; cursor: pointer;  }
		.estrella_dorada { color: gold; }
		.estrella_normal { color: black; }
    </style>
							
							<!-- Incluir jQuery -->
            <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
							
							<!-- Definir la función de puntuación -->
							<script type="text/javascript">
							function ratestar($id, $puntuacion){
								$.ajax({
									type: 'GET',
									url: 'votacion.php',
									data: 'votarElemento='+$id+'&puntuacion='+$puntuacion,
									success: function(data) {
										alert(data);
										location.reload();
									}
								});
							}
			</script>
</head>

<body>
	<!-- Start header -->
	<header class="top-navbar">
		<nav class="navbar navbar-expand-lg navbar-light bg-light">
			<div class="container">
				<a class="navbar-brand" href="index.html">
					<img src="images/logo2.png" alt=""   width="15%" height="15%"/>
				</a>
				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbars-rs-food" aria-controls="navbars-rs-food" aria-expanded="false" aria-label="Toggle navigation">
				  <span class="navbar-toggler-icon"></span>
				</button>
				<div class="collapse navbar-collapse" id="navbars-rs-food">
					<ul class="navbar-nav ml-auto">
						<li class="nav-item"><a class="nav-link" href="index.html">Home</a></li>
						<li class="nav-item active"><a class="nav-link" href="menu.html">Recetas</a></li>
						<li class="nav-item "><a class="nav-link" href="about.html">About</a></li>
						<li class="nav-item dropdown">
							<a class="nav-link dropdown-toggle" href="#" id="dropdown-a" data-toggle="dropdown">Pages</a>
							<div class="dropdown-menu" aria-labelledby="dropdown-a">
								<a class="dropdown-item" href="reservation.html">Reservation</a>
								<a class="dropdown-item" href="stuff.html">Stuff</a>
								<a class="dropdown-item" href="gallery.html">Gallery</a>
							</div>
						</li>
						
					</ul>
				</div>
			</div>
		</nav>
	</header>
	<!-- End header -->

	<?php

session_start();


$host_db = "localhost";
$user_db = "root";
$pass_db = "";
$db_name = "rpv";
$tbl_name = "receta";



$conexion = new mysqli($host_db, $user_db, $pass_db, $db_name);



if ($conexion->connect_error) {
 die("La conexion falló: " . $conexion->connect_error);
}


    $resultado = $conexion->query("SELECT nombre from $tbl_name where nombre='Pisto Manchego'");
	$resultado1 = $conexion->query("SELECT descripcion from $tbl_name where nombre='Pisto Manchego'");
	$resultado2 = $conexion->query("SELECT lista_ingredientes FROM receta  where nombre='Pisto Manchego'");


    ?>

   






	
	<!-- Start header -->
	<div class="all-page-title page-breadcrumb">
		<div class="container text-center">
			<div class="row">
				<div class="col-lg-12">


			<?php	
			
			if (mysqli_num_rows($resultado)>0){
    
				foreach ($resultado as $key => $fila) {
				 
					foreach ($fila as $nombreColumna => $dato) {
						
							echo "<h1>$dato</h1>";
						
				
					}
				   
				  
				}

					
			}

			?>


				</div>
			</div>
		</div>
	</div>
	<!-- End header -->
	
	<!-- Start About -->
	<div class="about-section-box">
		<div class="container">
			<div class="row">
				<div class="col-lg-6 col-md-6 text-center">
					<div class="inner-column">
					<h1><span> Receta</span></h1>
						
						<?php	
			
								if (mysqli_num_rows($resultado1)>0){
						
									foreach ($resultado1 as $key => $fila) {
									
										foreach ($fila as $nombreColumna => $dato) {
											
												echo "<p>$dato</p>";
											
									
										}
									
									
									}

										
								}

			?>
						
						
					</div>
				</div>
				<div class="col-lg-6 col-md-6">
					<img src="images/pisto-manchego.png" alt="" class="img-fluid">
				</div>
				<div class="col-md-12">
					<br>
					<br>
					<h1>Ingrediente (6 Personas)</h1>
					<div class="inner-pt">
						

							<?php	
								
								if (mysqli_num_rows($resultado2)>0){
						
									foreach ($resultado2 as $key => $fila) {
									
										foreach ($fila as $nombreColumna => $dato) {
											
												echo "<p>$dato</p>";
											
											
									
										}
									
									
									}

										
								}

							?>
							



							<?php


										$receta = "SELECT id from receta where nombre='Pisto Manchego'";
										$result= mysqli_query($conexion,$receta);
										$row=mysqli_fetch_assoc($result);



										// Incluir la clase Votacion desde el fichero votaciones.php
										include './votacion.php';

										// Activar un objeto de trabajo
										$V = new Votacion();
										?>




										<?php
										// Mostrar la valoración media


										// Mostrar las estrellas
										echo $V->mostrar_estrellitas_para( $row['id']);
										
									
										
										?>

<br>
<br>






<?php

$resultadoNombre = $conexion->query("SELECT nombre, mensaje from comentario where id_receta='10' ");


$numc= mysqli_num_rows($resultadoNombre);









	while ($resultName=mysqli_fetch_assoc($resultadoNombre)){



	
				
				?>
			
				<br>
				<br>
				
				<div class="comment-item">
				<div class="comment-item-left">
					<img src="images/avt-img.jpg" alt="">
				</div>
				<div class="comment-item-right">
					<div class="pull-left">
						<a href="#"><?php echo $resultName['nombre']?></a>
					</div>
					
					<div class="des-l">
						<p><?php echo $resultName['mensaje']?></p>
					</div>
					
				</div>
			</div>
	<?php
		}
	
	
	 

	?>
	





<br>
<br>
							<h3  >Escribe tu comentario</h3>
							
								<form id="commentrespondform" class="comment-form-respond row" method="post" action="comentarioInsertar.php" style="margin-rigth=190px;">
									
								<div class="col-lg-6 col-md-6 col-sm-6">
										<div class="form-group">
											<input id="nombre" class="form-control" name="nombre" placeholder="Nombre" type="text">
											</div>
											<div class="form-group">
											<input id="email" class="form-control" name="email" placeholder="Email" type="text">
											
											</div>

											<div class="form-group">
											<input id="id" class="form-control" name="id" placeholder="ID" type="text" value="10">
											</div>

										<div class="form-group">
											<textarea class="form-control" id="mensaje" name="mensaje" placeholder="Your Message" rows=""></textarea>
										</div>
									</div>
									<div class="col-lg-12 col-md-12 col-sm-12">
										<button class="btn btn-submit">Enviar Comentario</button>
									</div>
								</form>
						
					
















					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- End About -->
	
	<!-- Start Menu -->
	
	
	<!-- Start Contact info -->
	<div class="contact-imfo-box">
		<div class="container">
			<div class="row">
				<div class="col-md-4 arrow-right">
					<i class="fa fa-volume-control-phone"></i>
					<div class="overflow-hidden">
						<h4>Phone</h4>
						<p class="lead">
							+34 689789789
						</p>
					</div>
				</div>
				<div class="col-md-4 arrow-right">
					<i class="fa fa-envelope"></i>
					<div class="overflow-hidden">
						<h4>Email</h4>
						<p class="lead">
							rpv@gmail.com
						</p>
					</div>
				</div>
				<div class="col-md-4">
					<i class="fa fa-map-marker"></i>
					<div class="overflow-hidden">
						<h4>Location</h4>
						<p class="lead">
							<a href="https://www.google.com/maps/place/C.+San+Juan+de+la+Cruz,+8,+50006+Zaragoza/@41.6410285,-0.8947001,17z/data=!3m1!4b1!4m5!3m4!1s0xd5914d93dd842cf:0x7ab8ec15d1389db4!8m2!3d41.6410285!4d-0.8925114">nº8, San Juan de la Cruz, Zaragoza</a>
						</p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- End Contact info -->
	
	<!-- Start Footer -->
	
		
		
		<div class="copyright">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<p class="company-name">All Rights Reserved. &copy; 2018 Live Dinner Restauran Design By : 
					html design</p>
					</div>
				</div>
			</div>
		</div>
		

	<!-- End Footer -->
	
	<a href="#" id="back-to-top" title="Back to top" style="display: none;"><i class="fa fa-paper-plane-o" aria-hidden="true"></i></a>

	<!-- ALL JS FILES -->
	<script src="js/jquery-3.2.1.min.js"></script>
	<script src="js/popper.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
    <!-- ALL PLUGINS -->
	<script src="js/jquery.superslides.min.js"></script>
	<script src="js/images-loded.min.js"></script>
	<script src="js/isotope.min.js"></script>
	<script src="js/baguetteBox.min.js"></script>
	<script src="js/form-validator.min.js"></script>
    <script src="js/contact-form-script.js"></script>
    <script src="js/custom.js"></script>
</body>
</html>