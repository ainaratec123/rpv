-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 21-11-2021 a las 23:08:50
-- Versión del servidor: 10.4.8-MariaDB
-- Versión de PHP: 7.1.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `rpv`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comentario`
--

CREATE TABLE `comentario` (
  `id` int(11) NOT NULL,
  `nombre` varchar(24) NOT NULL,
  `email` varchar(50) NOT NULL,
  `mensaje` varchar(8000) NOT NULL,
  `id_receta` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `comentario`
--

INSERT INTO `comentario` (`id`, `nombre`, `email`, `mensaje`, `id_receta`) VALUES
(1, 'ainara', 'ainara@', 'prueba', 1),
(3, 'Ainara', 'ainaratec13@gmail.com', 'Muy buena receta, hecha y recomendada  para todos.', 15),
(4, 'diego', 'zapixmadrid@gmail.com', '8787\r\n', 15),
(5, 'prueba', 'zapixmadrid@gmail.com', '878', 1),
(10, 'prueba1 ', 'ainaratec@hotmail.com', 'sadas', 15),
(12, 'Ainara', 'ainaratec@hotmail.com', '8888', 0),
(17, 'lentejas', 'zapixmadrid@gmail.com', 'ASDASD', 1),
(18, 'fabada', 'ainaratec@hotmail.com', 'pruebaas', 4),
(19, 'caracoles', 'prueba@gmail.com', 'prueba caracoles', 5),
(20, 'paella', 'ainaratec@hotmail.com', 'prueba paella', 6),
(21, 'cochinillo', 'ainaratec@hotmail.com', 'prueba cochinillo\r\n', 7),
(22, 'manitas', 'zapixmadrid@gmail.com', 'manitas', 8),
(23, 'migas', 'ainaratec@hotmail.com', 'migas prueba', 9),
(24, 'pisto', 'zapixmadrid@gmail.com', 'pisto ', 10),
(25, 'croquetas', 'zapixmadrid@gmail.com', 'prueba croquetas', 11),
(26, 'alitas', 'ainaratec@hotmail.com', 'alitas', 12),
(27, 'tortilla', 'ainaratec@hotmail.com', 'prueba tortilla', 13),
(28, 'bacalao', 'ainaratec@hotmail.com', 'bacalao', 14);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `receta`
--

CREATE TABLE `receta` (
  `id` int(3) NOT NULL,
  `nombre` varchar(60) NOT NULL,
  `descripcion` mediumtext NOT NULL,
  `tipo_receta` varchar(24) NOT NULL,
  `lista_ingredientes` mediumtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `receta`
--

INSERT INTO `receta` (`id`, `nombre`, `descripcion`, `tipo_receta`, `lista_ingredientes`) VALUES
(1, 'Caldereta de Cordero', 'Comenzamos sazonando la carne y friéndola en aceite de oliva virgen extra hasta dorar bien su exterior. Después retiramos las tajadas de cordero y en el mismo aceite en el que hemos frito la carne preparamos un sofrito con la cebolla y el pimiento muy picados.\r\n\r\n\r\nCuando la cebolla esté transparente y el pimiento haya cambiado de color, añadimos las ramitas aromáticas, la hoja de laurel y un manojito de perejil picado, al que añadimos un poco de pimentón y tras dejarlo un minuto lo mezclamos después con los demás ingredientes.\r\n\r\n\r\nIncorporamos a continuación el tomate muy picado y sin semillas y removemos con el resto de ingredientes dejando que se cocine despacito hasta que no quede nada de líquido. En ese momento reintegramos las tajadas de cordero a la cazuela, añadimos el vino y dejamos que evapore el alcohol removiendo durante un minuto.\r\n\r\n\r\nCubrimos ya el guiso con el caldo y dejamos que se haga a fuego lento durante unos 35-40 minutos, hasta que la carne de cordero esté bien tierna y el aroma de este guiso tradicional inunde la casa. Si vamos a hacer una guarnición, es el momento de prepararla para que esté lista en el momento de servir nuestra caldereta.', 'Recetas tradionales', 'cordero\r\n\r\najo\r\n\r\nhierbas provenzales\r\n\r\ncebolla\r\n\r\ntomate\r\n\r\nvino blanco\r\n\r\npimiento rojo\r\n\r\ncaldo de carne\r\n\r\nlaurel\r\n\r\npimenton dulce\r\n\r\naceite de oliva virgen extra'),
(2, 'Cocido Madrileño', 'La noche anterior pondremos a remojo unos buenos garbanzos castellanos en la víspera del cocido. Truco: Además le pondremos un puñado de sal gorda para que al día siguiente no se encallen en la cocción. Empezaremos por poner a cocer, partiendo de agua fría, las carnes, la punta de jamón y los huesos indicados. Clave: Durante todo el cocido, de principio a fin, retiraremos la espuma que se vaya formando con una espumadera. Asimismo iremos incorporando agua según se vaya evaporando para que nuestro cocido no se quede seco.\r\n\r\nEl fuego del cocido lo tendremos de una forma continua a media potencia. Cuando el agua empiece a hervir, añadimos los garbanzos, previamente escurridos y lavados. Desde que el agua vuelva a hervir, tardarán en estar tiernos entre dos y tres horas, hecho a fuego lento o bien unos 20 minutos en caso de hacerlo en olla rápida. Recomendación: meted los garbanzos en una malla para poder sacarlos con facilidad al finalizar la cocción y así poder servir el cocido en los tres vuelcos tradicionales.\r\n\r\nEn un puchero aparte, ponemos a cocer el repollo, y en otra cacerola, cocemos chorizos y morcillas, para que no llenen de grasa nuestro caldo. Cuando el cocido esté prácticamente hecho, incorporamos las patatas y las zanahorias peladas en el puchero del cocido madrileño.\r\n\r\nAl finalizar el proceso, sacamos las carnes y las servimos en una fuente a la que incorporamos chorizos y morcillas. Rehogamos el repollo y lo ponemos en una fuente con los garbanzos, las patatas y las zanahorias. Para hacer la sopa, colamos el caldo y añadimos los fideos cuando el caldo empiece a hervir, siendo necesarios dos o tres minutos para los fideos finos tipo cabellín.', 'Recetas slow food', 'Garbanzos\r\n\r\nMorcillo\r\n\r\nTocino\r\n\r\nHueso de rodilla de ternera\r\n\r\nHuesos de espinazo de cerdo salado\r\n\r\nHueso de caña con tuétano\r\n\r\nChorizo fresco\r\n\r\nFideos cabellín o gruesos\r\n\r\nGallina\r\n\r\nPatatas\r\n\r\nZanahoria\r\n'),
(3, 'Callos a la Madrileña', 'Los callos a la madrileña se hacen normalmente utilizando varios tipos de carne de la casquería. Así entre sus ingredientes encontramos callos de ternera, pata y morro. En la casquería podemos pedir un kilo de callos indicando que queremos ese peso repartido entre los tres ingredientes. Limpiamos bien los callos y los cortamos en trozos cuadrados de 2x2 cm aproximadamente, para que después de su cocción puedan ser tamaño bocado.\r\n\r\nUna vez limpios, los ponemos a cocer con una hoja de laurel y un buen puñado de sal, agregando también la panceta o carne de jamón o lacón y las especias para callos. Dejamos que los callos se hagan durante 4 horas en cacerola tradicional o 45 minutos si usáis olla rápida. Una vez estén cocinados, los escurrimos y reservamos el agua de la cocción que será espesa y cargada de gelatina.\r\n\r\nCuando falte una hora para terminar la cocción tradicional, agregamos el chorizo y la morcilla, que habremos semicocido aparte. Al integrarlos ahora, dejando que se terminen de hacer con los demás ingredientes evitaremos que se reviente la morcilla o que la receta nos quedase demasiado cargada de la grasa del chorizo.\r\n\r\nPreparamos una salsa pochando dos dientes de ajo con la cebolla muy picada y una guindilla para que los callos tengan un puntito picante. Entonces agregamos una cucharada de salsa de tomate, los callos cocidos, los chorizos y morcillas troceados. También vamos incorporando poco a poco, el caldo de la cocción. Terminamos dejando hervir el conjunto durante 20 minutos hasta que adquiera cremosidad y llegue a una textura deseada.', 'Recetas slow food', 'Callos de ternera\r\n\r\nChorizo asturiano\r\n\r\nMorcilla de cebolla\r\n\r\nPanceta\r\n\r\nLaurel\r\n\r\nMezcla especias para callos al gusto al gusto\r\n\r\nDientes de ajo\r\n\r\nGuindilla al gusto\r\n\r\nSalsa de tomate\r\n'),
(4, 'Fabada Asturiana', 'La noche anterior comenzaremos dejando las habas en remojo en un recipiente grande pues crecen bastante, por otra parte en otro bol pondremos a desalar nuestro lacón.\r\n\r\nAl día siguiente echamos las habas en una olla de fondo grueso, añadiremos los dientes de ajo, la cebolla entera pelada, el aceite de oliva y la hoja de laurel, cubriremos con agua, mejor mineral, hasta dos dedos por encima de nuestras fabes, dejando que lleguen a la ebullición y cortando la cocción con un chorro de agua fría. Esto es lo que se dice “asustar les fabes”, operación que repetiréis a lo largo de la cocción otras dos veces más.\r\n\r\nMientras, lo que yo hago habitualmente, es en otra olla pequeña darle un hervor por separado a la carne, esto hace que sobre todo la morcilla suelte parte de su grasa y no nos quede con tanta al final la fabada, este paso es totalmente prescindible, si no os importa el exceso de grasa ese día, añadiréis toda la carne a la olla donde se cuecen las habas una vez que las hayamos asustado.\r\n\r\nSeguir cociendo a fuego medio-bajo durante tres horas aproximadamente, desespumando si es necesario y observando que no se quede seca, en el caso de ser así, vais añadiéndole pequeñas cantidades de agua caliente, bien sola o bien de la olla donde en un principio habíamos escaldado las carnes, acordándose otras dos veces de asustar la fabada con agua fría a lo largo del tiempo de cocción.\r\n\r\nJusto cuando las fabes ya están cocidas y ha pasado el tiempo, comprobar la sal y añadirle la que necesite, dejando cocer la fabada unos minutos para que la sal se incorpore. No echarla al \r\nprincipio sino en este momento.', 'Recetas slow food', 'Fabes de La Granja\r\n\r\nCebolla\r\n\r\nDientes de ajo\r\n\r\nChorizo asturiano\r\n\r\nMorcilla asturiana\r\n\r\nLacón salado\r\n\r\nTocino\r\n\r\nLaurel hoja\r\n\r\nAceite de oliva virgen extra\r\n'),
(5, 'Caracoles', 'Primero de todo tenemos que lavar bien los caracoles. Dos días antes hay que dejarlos con un poco de agua y harina para que se purguen bien. Si tienes alguna hierba aromática también ponla.\r\n\r\nPara lavar bien los caracoles, hay que hacerlo con abundante agua fría y un puñado de sal (no mucha). Hay quienes optar también por echarles un chorrito de vinagre. Debes hacerlo con cuidado de no lastimar las conchas y repetir el proceso hasta que el agua salga limpia.\r\n\r\nA continuación ponlos en una olla cubiertos de agua fría y cuando salgan de la concha, subimos el fuego hasta que den el primer hervor.\r\n\r\nCuando hayan empezado a hervir, espumamos, le tiramos ese agua y le ponemos nueva que esté ya caliente. Dejamos que hiervan otra vez. Repite el proceso una vez más.\r\n\r\nEn el tercer hervor ya dejamos que cuezan unas dos horas aproximadamente y añadimos en la olla una cebolla cortada a cuartos, los ajos y un puñado de sal.\r\n\r\nDurante la cocción de los caracoles, prepararemos el sofrito. Para ello, en una sartén sofreímos la otra cebolla cortada muy finita, junto con el chorizo y el jamón.\r\n\r\nDespués añadimos los tomates rallados, la cayena y unos granos de comino. Dejamos que se haga la salsa y cuando esté lista, incorporamos los caracoles y un vaso de caldo de carne. Rectificamos de sal.\r\n\r\nCocemos los caracoles a la madrileña durante un cuarto de hora aproximadamente hasta que reduzca el caldo y los caracoles hayan cogido todo el sabor de la salsa. ¡Ya están listos para degustar!', 'Recetas slow food', 'caracoles congelados\r\n\r\njamón serrano\r\n\r\nchorizo\r\n\r\ncebolla\r\n\r\ndientes de ajo\r\n\r\ntomates\r\n\r\ncucharadita de pimentón dulce\r\n\r\ncucharadita de pimienta de cayena\r\n\r\ncucharadita de hierbabuena\r\n\r\nhojas de laurel\r\n\r\nrodajas de pan de molde\r\n\r\nSal\r\n\r\nAceite de oliva'),
(6, 'Paella Valenciana', 'Toda paella que se precie comienza por un buen sofrito. En una paella cuanto más grande mejor, se sofríe en abundante aceite el pollo, el conejo, las judías, las alcachofas y los caracoles (la que veis en la foto no tiene garrofó porque no es temporada y el congelado no es igual), sazonando con un poco de sal y pimentón hacia el final. Cuando esté bien dorado se añade el tomate triturado y se rehoga.\r\n\r\nCon el sofrito listo se debe de añadir el agua. Las proporciones dependen mucho del fuego, del calor que haga, del grado de humedad y de lo grande que sea la paella, pero para comenzar, una buena proporción es la de añadir tres veces el volumen de agua que de arroz, aunque es la experiencia la que os hará ajustar y perfeccionar estas cantidades, que acabaréis haciendo a ojo, como hicieron la tía y la madre de mi novia, que eran las encargadas de esta paella (a pesar de que la tradición marca que sea el hombre de la casa el que la prepare).\r\n\r\nEchamos ahora algunos troncos más al fuego para que suba de potencia y se haga bien el caldo durante 25 o 30 minutos. Es un buen momento de echar el azafrán o, en su defecto, el sazonador de paella (el más popular es \"el paellador), que lleva sal, ajo, colorante y un poco de azafrán.\r\n\r\nLuego añadimos el arroz \"en caballete\" (en diagonal) y lo distribuimos por la paella. Cocemos entre 17 y 20 minutos, aunque aquí el tiempo lo marca de nuevo el grano de arroz y la potencia del fuego, que debemos ir dejando consumirse. Tiene que quedar completamente seco y suelto. Mi recomendación para los primerizos es que tengáis un cazo con agua hirviendo al lado, por si hay que añadir agua. A mitad cocción también podemos poner unas ramitas de romero, que retiraremos antes de servir.\r\n\r\nPor último, conviene dejar la paella reposar unos minutos tapada con un gran paño o papel de periódico --no es bueno porque con la humedad se puede liberar algo de tinta, pero toda la vida lo he visto usar-- antes de servirla y recibir el aplauso de los presentes.', 'Recetas tradionales', 'Arroz bomba\r\n\r\nPollo de corral\r\n\r\nConejo\r\n\r\nJudía verde plana\r\n\r\nGarrofó\r\n\r\nAlcachofa (opcional)\r\n\r\nCaracoles\r\n\r\nAceite de oliva virgen extra\r\n\r\nPimentón dulce\r\n\r\nTomate triturado\r\n\r\nAzafrán\r\n\r\nRomero fresco\r\n\r\nSal'),
(7, 'Cochinillo Asado', 'Pedimos al carnicero que nos elija un cochinillo lechal pequeño, a ser posible que no pase de 4 kg o no nos va a caber abierto en el horno. Si no nos cabe, podemos cortarle los dos jamones y asarlos en otra bandeja como tuve que hacer yo, si os fijáis en el collage.\r\n\r\nPrecalentamos el horno a 190ºC y mientras salpimentamos el cochinillo o lechón y lo untamos con la manteca de cerdo. Lo colocamos con las costillas hacia arriba sobre una bandeja de horno y lo horneamos 90 minutos a 190ºC con función vapor. Si no tenéis esa función en vuestro horno, podéis hornearlo sobre la rejilla y poner debajo una bandeja llena de agua y os quedará perfecto.\r\n\r\nTras los 90 minutos, damos la vuelta al cochinillo con cuidado y dejamos que se ase otros 30 o 40 minutos sin tocar la temperatura. Así se irá dorando muy despacio la piel y la grasa se fundirá manteniendo muy tierno nuestro asado. Finalmente, gratinamos durante 4 minutos para conseguir un dorado perfecto y una piel crujiente.', 'Recetas tradionales', 'Cochinillo lechal \r\n\r\nManteca de cerdo\r\n\r\nAgua\r\n\r\nSal \r\n\r\npimienta al gusto'),
(8, 'Manitas de cordero', 'Retira los tallos y las pepitas de los pimientos choriceros. Ponlos en un cazo, cúbrelos con agua y cuécelos durante 10 minutos. Retira los pimientos choriceros y retírales la pulpa.\r\n\r\nPon agua a calentar en la olla rápida, agrega las manitas y el chorizo. Pela la zanahoria e incorpórala (entera). Sazona, coloca la tapa y cuece todo durante 25 minutos a partir del momento en que suba la válvula. Saca las manitas, la zanahoria y el chorizo y resérvalos. Con un cazo elimina del caldo la mayor parte de la grasa que haya soltado el chorizo. Resérvalo.\r\n\r\nPela los ajos y las cebollas, pícalos en dados y ponlos a pochar en una cazuela con un chorrito de aceite. Añade la harina y rehógala un poco. Incorpora la cayena, la zanahoria troceada, la salsa de tomate y la misma cantidad de caldo (desgrasado) resultante de cocer las manitas. Añade también la carne de los pimientos choriceros y cocina los ingredientes de la salsa durante 20 minutos a fuego suave. Retira la cayena y tritura hasta conseguir una salsa homogénea.\r\n\r\nPasa la salsa a una tartera, corta el chorrizo en rodajas y agrégalas. Incorpora también las manitas y cocínalas en la salsa durante 10 minutos. Sirve y adorna con unas hojas de perejil.', 'Recetas tradionales', 'manitas de cordero lechal\r\n\r\ncebollas\r\n\r\nzanahorias\r\n\r\npimiento verde italiano\r\n\r\npimiento rojo\r\n\r\ndientes de ajo\r\n\r\nvaso de vino blanco (fino)\r\n\r\nhojas de laurel\r\n\r\npimentón dulce de la Vera (opcional)\r\n\r\nPimienta negra molida\r\n\r\nOrégano\r\n\r\naceite de oliva virgen extra\r\n\r\nSal\r\n\r\nHarina fina de maíz (opcional)'),
(9, 'Migas del Pastor', 'La cocina tradicional, en este caso de Aragón y Extremadura, cuenta en su haber con recetas que son tremendamente humildes, pero deliciosas. Entre ellas se encuentran las migas del pastor, un plato elaborado con pan, chorizo, panceta y ajo que se completa con huevo frito. ¿Acaso hay algo que suene mejor? Posiblemente, no.\r\n\r\nMás puntos a favor de esta receta es su bajo coste, lo fácil que es de preparar y que está lista en media hora. Además es una manera estupenda de aprovechar restos de pan del día anterior, así que seguimos sumando bondades a la criatura. Nosotros las hemos preparado con pan candeal, pero la verdad es que cualquier otro sirve.\r\n\r\nRemovemos sin parar durante 20 minutos a fuego medio-bajo, para que las migas se impregnen bien de la grasa y el aroma de los ajos al tiempo que se van secando. Añadimos la panceta y el chorizo, espolvoreamos con el pimentón, removemos e, inmediatamente, retiramos del fuego. Freímos los huevos y servimos las migas con ellos.', 'Recetas tradionales', 'Pan tipo candeal\r\n\r\nAgua\r\n\r\nChorizo fresco\r\n\r\nPanceta\r\n\r\nDientes de ajo\r\n\r\nPimentón dulce \r\n\r\nSal\r\n\r\nHuevos\r\n'),
(10, 'Pisto Manchego', 'Calentamos agua en una cacerola amplia y la llevamos a ebullición. Retiramos la parte dura de los tomates y practicamos un corte de cruz en la base. Los introducimos en el agua hirviendo durante unos 15-20 segundos, los sacamos y los introducimos en un cuenco de agua helada. Los pelamos y trituramos.\r\n\r\nPelamos los dientes de ajo y la cebolla y los picamos finamente. Lavamos bien el resto de las verduras y las cortamos en pequeños trozos de igual tamaño. Las reservamos separadas unas de otras pues las iremos añadiendo a la cazuela en distintas fases.\r\n\r\nCalentamos una cantidad generosa de aceite de oliva virgen extra en una cazuela y pochamos el ajo y la cebolla picados durante 15 minutos a fuego suave. Añadimos el pimiento picado y pochamos 15 minutos más. Por último incorporamos el calabacín y el tomate triturado, salpimentamos al gusto, tapamos y dejamos pochar durante un mínimo de una hora y media.\r\n\r\nTranscurrido este tiempo, retiramos la tapadera de la cazuela, subimos un poco el fuego y cocemos durante 15 minutos más o hasta que el agua del tomate se haya evaporado. Queremos que nos quede jugoso, pero sin restos del agua que sueltan las verduras, con todos los ingredientes bien amalgamados. una vez conseguida esta consistencia, servimos inmediatamente.', 'Recetas tradionales', 'Dientes de ajo\r\n\r\nCebolla\r\n\r\nPimiento verde\r\n\r\nPimiento rojo\r\n\r\nTomate\r\n\r\nCalabacín\r\n\r\nSal\r\n\r\nPimienta negra molida\r\n\r\nAceite de oliva virgen extra'),
(11, 'Croquetas de espinacas y queso azul', 'Para hacer las croquetas, lo primero que necesitamos es cocer las espinacas durante 5 minutos en agua con sal. Luego las escurrimos a conciencia para que tiren todo el agua, dejándolas unos minutos sobre un colador. Una vez escurridas, ponemos una sartén al fuego y derretimos la mantequilla.\r\n\r\nRehogamos ligeramente las espinacas en la mantequilla y añadimos dos cucharadas de harina, removiendo hasta que sea absorbida por las espinacas. A continuación, vamos añadiendo poco a poco la leche, dejando que las espinacas la absorban antes de añadir más. Removemos y añadimos leche hasta obtener una bechamel verde que sea espesa como para hacer croquetas.\r\n\r\nAñadimos el queso azul y removemos para que se funda con el calor residual y se reparta bien en toda la pasta. Una vez disuelto, dejamos reposar la pasta de croquetas en una fuente hasta que se enfríe. Para formar las croquetas, en esta ocasión he formado bolas que pasamos por huevo batido y pan rallado.\r\n\r\nUna vez formadas, podemos congelar las croquetas hasta el día que las vayamos a freír o freírlas directamente en aceite de oliva muy caliente, dejando las croquetas un par de minutos por cada lado hasta que empiecen a tomar un color tostado como el de la foto de presentación. Luego, solo falta escurrirlas en papel absorbente y llevarlas a la mesa muy calientes.', 'Receta freidora de aire', 'Leche \r\n\r\nEspinaca fresca o congeladas\r\n\r\nMantequilla\r\n7\r\nHarina de trigo\r\n\r\nQueso azul\r\n\r\nHuevo para envolver las croquetas\r\n\r\nPan rallado para envolver las croquetas'),
(12, 'Alitas de Pollo', 'Esta receta se puede hacer con tantas alitas como quepan en la superficie de la bandeja de tu freidora sin aceite. Ahora bien, solo pueden ocupar una capa. En ningún caso pueden estar superpuestas o no lograremos que queden crujientes. Esto limita mucho la cantidad de alitas que podemos hacer, que en muchos casos no pasarán de tres o cuatro.\r\n\r\nSi las alitas no están partidas, pártelas en dos mitades. Colócalas en un bol y condiméntalas con pimenton dulce o picante, unas vueltas de pimientra negra recién molida, una pizca de sal y ajo granulado al gusto. Asegurate de que las alitas estén bien embardurnadas por las especias en todos lados.\r\n\r\nDispón las alitas en la bandeja de la freidora sin aceite sin que se superpongan, y prográmala a 180º C durante 15 minutos. Pasado este tiempo, cocínalas a 200º durante 5 minutos. Extrae las alitas con ayuda de unas pinzas (con cuidado de que no entren en contacto con la grasa que quedará al fondo) y sirve de inmediato.', 'Receta freidora de aire', 'Alitas de pollo\r\n\r\nPimentón dulce o picante\r\n\r\nSal\r\n\r\nPimienta negra molida\r\n\r\nAjo granulado'),
(13, 'Tortilla de Patatas', 'Pelamos y troceamos las papas. (A nosotros nos gustan muy finitas, y favorece que se cocinen en la freidora).\r\n\r\nLas lavamos bien para quitar el almidón. En un bol añadimos una cucharada de aceite y la mezclamos bien con las patatas, esto evitará que se peguen y las mantendrá hidratadas.\r\n\r\nMetemos las patatas en la freidora y calentamos a 180º durante unos 30 min. Si tu freidora tiene un programa de patatas aprovéchalo.\r\n\r\nRemuévelas cada 5-10 min para que se hagan de forma uniforme si no tiene sistema automático de mezclado. Si lo has de remover a mano quizás tengas que añadirle unos minutos más a ojo por la pérdida de calor.\r\n\r\nCuando vayas moviéndolas añadid la sal, para que se vaya mezclando bien. Batimos los huevos. En este caso hemos usado 7 huevos.\r\n\r\nCuando las patatas estén listas las sacamos. (sabremos que están listas porque se quedan apelotonadas y jugosas). Le añadimos el huevo batido. Metemos la mezcla de patata y huevo en la freidora. Y con el menú manual. Durante 6-8 minutos a 130º la cuajaremos. Emplatamos y solamente queda disfrutar.\r\n\r\n', 'Receta freidora de aire', 'patatas.\r\n\r\naceite de oliva virgen extra\r\n\r\nSal al gusto.\r\n\r\nhuevos'),
(14, 'Bacalao Rebozado', 'En primer lugar, limpiamos bien el pescado, retirando la piel y las espinas que pueda tener, y lo cortamos en trozos grandes. Lo salpimentamos bien por todas partes. Si es bacalao desalado, mejor no echar más sal.\r\n\r\nEn tres platos hondos, preparamos las tres capas por las que va a pasar el pescado. En el primero, mezclamos la harina de trigo, la de maiz, el ajo en polvo, el pimentón y un poco de pimienta negra recién molida. En el segundo, batimos los dos huevos y echamos las dos cucharadas de agua y un par de chorritos de alguna salsa picante, como Tabasco, si nos gusta. En el tercer plato ponemos el panko.\r\n\r\nDe pieza en pieza, pasamos el pescado por la harina, el huevo y, al finalizar, el panko, que podemos presionar para que quede bien adherido al pescado. Vamos colocando las piezas en la bandeja de la freidora de aire caliente, sin que se peguen ni se superpongan. Dependiendo del tamaño de tu electrodoméstico, las cantidades de esta receta se tienen que cocinar en dos, tres o, incluso, cuatro tandas.\r\n\r\nEchamos unas gotas de aceite sobre el pescado (ideal para esta receta si contamos con aceite de oliva en espray) y lo cocinamos a 200º C durante 5 minutos. Pasado este tiempo, con cuidado de no romper el rebozado, damos la vuelta a las piezas, y las cocinamos otros 5 minutos a la misma temperatura. Pasado este tiempo estarán perfectamente doradas y listas para consumir.\r\n\r\n\r\n', 'Receta freidora de aire', 'bacalao desalado en lomos\r\n \r\nleche\r\n\r\najo\r\n\r\nAceite de oliva virgen extra\r\n\r\nharina\r\n\r\n\r\nhuevo\r\n\r\npimenton\r\n\r\npimienta'),
(15, 'Merluza', 'A las cuatro piezas de pescado le agregamos un chorrito de aceite de oliva en la parte superior. Acto seguido le ponemos al gusto el perejil picado, el ajo y la sal.\r\n\r\nPonemos las piezas del pescado en la cubeta o en un recipiente apto para la airfryer. Seleccionamos el programa pescado.\r\n\r\nSi en tu freidora no tienes este programa, configurar la temperatura a 200 grados durante 10 minutos. Una vez terminado el programa, lo servimos en un plato.', 'Receta freidora de aire', 'perejil\r\n\r\nmerluza\r\n\r\najo granulado\r\n\r\naceite de oliva virgen extra\r\n\r\nsal'),
(16, 'lentejas', 'Picamos en trozos menudos el ajo, la cebolla, la zanahoria, el pimiento verde y el puerro. Calentamos un poco de aceite de oliva en una cacerola, añadimos las verduras y sofreímos a fuego bajo durante cinco minutos. Sazonamos ligeramente, agregamos la salsa de tomate y cocer tres minutos más.\r\nAñadimos las lentejas, la hoja de laurel y las patatas, chascándolas con el cuchillo, y cocemos un par de minutos. Cubrimos con abundante agua fría y cocemos (mejor tapadas) durante 40-50 minutos o hasta que estén tiernas. Si se vieran muy secas, añadimos más agua durante el proceso.\r\n\r\nCalentamos un chorro de aceite en una sartén, añadimos el pimentón y retiramos inmediatamente del fuego para evitar que se queme. Incorporamos a la cacerola con las lentejas, removemos y cocemos cinco minutos más. Servimos bien calientes.', 'Recetas tradionales', 'Lentejas\r\n\r\nPimiento verde\r\n\r\nCebolla\r\n\r\nPuerro\r\n\r\nZanahoria\r\n\r\nPatata\r\n\r\nSalsa de tomate casera\r\n\r\nAceite de oliva virgen extra \r\n\r\nDiente de ajo\r\n\r\nLaurel\r\n\r\nSal\r\n\r\nPimentón dulce '),
(17, ' fideuá de pulpo', 'Comenzamos dorando los fideos en la paella, no demasiado, removiendo de vez en cuando para que unos tomen más color que otros. Mientras hacemos esta tarea, cocemos el pulpo sin añadir agua, en una olla rápida, durante 10 minutos. Así obtendremos el caldo hacer la fideuá. Como necesitaremos aproximadamente 750 ml de caldo, y de la cocción del pulpo no conseguiremos tanta cantidad, completamos añadiendo caldo de pescado o un caldo de morralla casero que tengamos.\r\n\r\nDoramos las hebras de azafrán durante 20 segundos en el microondas o las tostamos ligeramente con el método que os contamos aquí. Calentamos una taza de caldo y deshacemos el azafrán con los dedos echandolo dentro. Después lo mezclamos con el resto del caldo y lo dejamos en infusión hasta que lo vayamos a usar. Salteamos las patas de pulpo cocidas en la paella con un poco de aceite. Después, cortamos en rodajas las partes más gruesas, dejando las puntas de los tentáculos de una pieza, para adornar por encima al terminar la elaboración.\r\n\r\nEn la paella, ponemos tres o cuatro cucharadas de nuestro sofrito casero a calentar. Después, apagamos el fuego, y tras unos segundos, echamos el pimentón y mezclamos bien. Encendemos el fuego de nuevo, y añadimos los fideos y las rodajas de pulpo. Mezclamos bien durante dos o tres minutos, mientras llevamos el caldo a ebullición en una cacerola.\r\n\r\n\r\nEs el momento de añadir el caldo a la paella, repartir bien los fideos y colocar las patas o tentáculos. Cocinamos a fuego fuerte hasta que los fideos absorban el caldo. Para fideos entrefinos, serán necesarios unos 13 minutos. Si como yo optáis por el fideo cabellín, podéis reducir la cantidad de caldo un poco y dejar cocer unos 8 minutos, calculando según las indicaciones del fabricante de la pasta. Dejamos reposar un par de minutos la fideuá y la llevamos a la mesa.', 'Recetas slow food', 'Fideos tipo cabellín o entrefinos\r\n\r\nSofrito\r\nCaldo de pescado o fumet\r\n\r\nPulpo fresco aproximadamente\r\n\r\nAzafrán unas hebras\r\n\r\nPimentón dulce \r\n\r\nAceite de oliva virgen extra '),
(18, 'albondigas', 'En un recipiente hondo y amplio mezclamos el huevo con la leche, la mostaza y el ketchup. Batimos bien y agregamos las dos rebanadas de pan de molde troceadas. Aplastamos con un tenedor hasta obtener una papilla homogénea en la que no se noten trozos de pan. Añadimos las hierbas provenzales, la sal de ajo y salpimentamos al gusto.\r\n\r\nAgregamos la carne picada y mezclamos con las manos (bien limpias) para que se integren todos los ingredientes y sabores. Dejamos reposar la mezcla una hora en la nevera, con el recipiente bien tapado. Mientras tanto, preparamos una salsa de tomate casera. No la compréis preparada, este es un punto muy importante para conseguir que nuestras albóndigas pasen de estar ricas a ser, sencillamente, \"las albóndigas definitivas\".\r\n\r\nCuando la mezcla de las carne haya reposado, formamos las albóndigas. Tomamos pequeñas porciones, las boleamos y las pasamos por harina. Tardaremos un poco más si las hacemos pequeñas, pero el resultado es más delicado y después se cocerán más rápido en la salsa. Esta es una masa blanda, pero se deja manejar y da como resultado unas albóndigas tiernas en su interior y muy jugosas.\r\n\r\nUna vez rebozadas las albóndigas, calentamos abundante aceite de oliva virgen extra en una sartén y las freímos a fuego fuerte durante un minuto. Queremos que queden selladas por el exterior, así que las volteamos con cuidado al freír. Escurrimos las albóndigas sobre papel absorbente antes de pasarlas a uan cazuela con la salsa de tomate y cocer durante 10 minutos. Servimos inmediatamente.', 'Recetas tradionales', 'Ternera\r\n400 g\r\nPan de molde rebanadas\r\n2\r\nHuevo\r\n1\r\nLeche\r\n\r\nMostaza de Dijon\r\n\r\nKetchup\r\n\r\nHierbas provenzales\r\n\r\nSal\r\n\r\nPimienta negra molida\r\n\r\nHarina de trigo\r\n\r\nAceite de oliva virgen extra para freír\r\n\r\nSalsa de tomate casera\r\n'),
(19, 'Espaguetis con pesto de almendras', 'Tostar las almendras en una sartén sin aceite a potencia media, removiéndolas constantemente para que no se quemen, junto con un diente de ajo. Si vamos a usar el horno para otra cosa, prepararlas en él a 200ºC con calor arriba y abajo, 12 minutos. Dejar enfriar un poco.\r\n\r\nTriturar en un procesador de alimentos, robot o batidora las almendras tostadas, los dos dientes de ajo, la albahaca lavada y seca, una cucharadita rasa de sal, el aceite de oliva virgen extra y un poco de ralladura de limón. Debe quedar una pasta grumosa, con textura. Probar y ajustar de sal.\r\n\r\nPoner a cocer los espaguetis con abundante agua salada hirviendo siguiendo las instrucciones del paquete, pero sacándolos un minuto antes de estar al dente. Mientras se cocinan, poner la picada en una cazuela o sartén amplia y añadir los tomates rallados sin la piel.\r\n\r\nCuando la pasta casi esté, sacar una taza del agua de cocción; escurrir los espaguetis y añadir a la salsa. Mezclar bien e incorporar poco a poco parte del agua que se ha retirado, removiendo, cocinando a fuego suave, hasta integrar. Debe quedar reducido y meloso.\r\n\r\nApagar el fuego y echar finalmente un buen manojo de rúcula, removiendo suavemente. Repartir en platos y servir con algo más de rúcula fresca y/o albahaca, y ralladura de limón', 'Recetas tradionales', 'Almendras crudas con piel\r\n\r\nDiente de ajo pelados\r\n\r\nAlbahaca en hojas \r\n\r\nAceite de oliva virgen extra \r\n\r\nTomate frescos o unos 400 en lata\r\n\r\nRúcula un par de manojos\r\n\r\nSal\r\n\r\nRalladura de limón\r\n\r\nEspaguetis\r\n'),
(20, 'sopa juliana', 'Lavamos todas las verduras y pelamos las que sea necesario. En nuestro caso as zanahorias, el nabo y el puerro, al que retiramos la capa exterior. Cortamos todas ellas en juliana, es decir, en tiras finas y largas. Si contamos con una mandolina la tarea es coser y cantar, si no es el caso, cuchillo, tabla y un pelín de paciencia.\r\n\r\nCalentamos el aceite en una cacerola amplia y rehogamos las verduras, todas juntas, durante unos diez minutos. A continuación añadimos el caldo de verduras casero y cocemos 15 minutos, lo suficiente para que las verduras queden al dente.\r\n\r\nSi os gustan las verduras muy tiernas, las dejáis unos minutos más. En cualquier caso, una vez finalizada la cocción probamos el punto de sal y pimienta y ajustamos al gusto. Servimos la sopa bien caliente decorando cada plato con unas hojitas de perejil (opcional).', 'Recetas tradionales', 'Puerro\r\n\r\nZanahoria\r\n\r\nRepollo\r\n\r\nApio rama\r\n\r\nNabo\r\n\r\nAceite de oliva virgen extra\r\n\r\nSal\r\n\r\nPimienta negra molida\r\n\r\nCaldo de verduras casero\r\n');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE `usuario` (
  `username` varchar(20) NOT NULL,
  `password` varchar(20) NOT NULL,
  `nombre` varchar(20) NOT NULL,
  `apellido1` varchar(20) NOT NULL,
  `apellido2` varchar(20) NOT NULL,
  `email` varchar(50) NOT NULL,
  `edad` int(3) NOT NULL,
  `telefono` int(9) NOT NULL,
  `tipo_usuario` varchar(20) NOT NULL,
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`username`, `password`, `nombre`, `apellido1`, `apellido2`, `email`, `edad`, `telefono`, `tipo_usuario`, `id`) VALUES
('ainaratec13', '1234567', 'ainara', 'tercero', 'campanero', 'ainaratec13@gmail.com', 22, 680152099, '', 1),
('Administrador', 'admin', 'Administrador', 'Administrador', 'Administrador', 'administrador@administrador.com', 0, 0, 'administrador', 2),
('pepe', 'pepe', 'prueba', 'Tercero', 'Campanero', 'zapixmadrid@gmail.com', 0, 680152099, 'administrador', 3),
('ainara', '1234567', 'Ainara', 'Tercero', 'Campanero', 'zapixmadrid@gmail.com', 22, 680152099, 'administrador', 7);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `votaciones`
--

CREATE TABLE `votaciones` (
  `elemento_votado` int(10) DEFAULT NULL,
  `valoracion` int(10) DEFAULT NULL,
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `votaciones`
--

INSERT INTO `votaciones` (`elemento_votado`, `valoracion`, `id`) VALUES
(1, 4, 1),
(2, 3, 2),
(3, 5, 3),
(1, 4, 5),
(1, 5, 11),
(1, 5, 12),
(1, 5, 14),
(1, 5, 16),
(1, 1, 17),
(2, 5, 18),
(2, 1, 19),
(1, 1, 20),
(6, 3, 21),
(3, 1, 22),
(4, 5, 23),
(5, 3, 24),
(7, 3, 25),
(8, 4, 26),
(9, 1, 27),
(10, 3, 28),
(11, 1, 29),
(12, 5, 30),
(13, 4, 31),
(14, 3, 32),
(15, 5, 33),
(1, 5, 34),
(1, 4, 35);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `comentario`
--
ALTER TABLE `comentario`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `receta`
--
ALTER TABLE `receta`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `votaciones`
--
ALTER TABLE `votaciones`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `comentario`
--
ALTER TABLE `comentario`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT de la tabla `receta`
--
ALTER TABLE `receta`
  MODIFY `id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT de la tabla `usuario`
--
ALTER TABLE `usuario`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `votaciones`
--
ALTER TABLE `votaciones`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
